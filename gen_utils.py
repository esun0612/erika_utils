#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 14:53:18 2019

@author: esun
"""
import json
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf

def default(obj):
    """Workaround for the issue to dump numpy.int64 into json string in Python 3
    """
    if isinstance(obj, np.int64): 
        return int(obj)  
        
def save_json(obj, json_file):
    with open(json_file, "w") as f:
        json.dump(obj, f, indent=4, default=default)
        
def save_fig_to_pdf(fig, pdf_file):
    """Save fig to pdf file
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdf_file)
    pdf.savefig(fig)
    pdf.close() 