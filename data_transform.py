#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 15:02:40 2019

@author: esun
"""
import json
import pickle
from collections import OrderedDict
import numpy as np


class BaseProcess():
    """Base class for all pre-processing class
    """
    def __init__(self, json_file='', pickle_file='', cols=''):
        """Define header files for data processing
        pre_process_header is used in fit and fit_transform
        json_file can be generated in fit and fit_transform and should be used in transform and inverse_transform
        """
        self.json_file = json_file
        self.pickle_file = pickle_file
        self.cols = cols
           
    def save_json(self):
        with open(self.json_file, "w") as f:
            json.dump(self.stats, f, indent=4, default=self.default)
    
    def default(self, o):
        """Workaround for the issue to dump numpy.int64 into json string in Python 3
        """
        if isinstance(o, np.int64): 
            return int(o)  

    def load_json(self):
        with open(self.json_file, "r") as f:
            self.stats = json.load(f, object_pairs_hook=OrderedDict)    
            
    def save_pickle(self):
        with open(self.pickle_file, 'wb') as f:
            pickle.dump(self, f)
    
    def load_pickle(self):
        with open(self.pickle_file, 'rb') as f:
            return pickle.load(f)
    
    def transform(self, df):
        if self.pickle_file != '': 
            self.stats = self.load_pickle().stats
        else:
            self.load_json()
        self.cols = [x for x in self.stats.keys()]
        
class PandasCapping(BaseProcess):
    """Variable capping at a given value or given percentile
    """
    def __init__(self, json_file='', pickle_file='', cols='', cap_p=0.99):
        super().__init__(json_file, pickle_file, cols)
        self.cap_p = cap_p
        
    def fit_transform(self, df):
        self.stats = OrderedDict()
        data = df.copy()
        for col in self.cols:
            cap_val = data[col].quantile(self.cap_p)
            data[col] = np.clip(data[col], -np.nan, cap_val)
            self.stats[col] = cap_val
        self.save_json()
        self.save_pickle() 
        return data
    
    def transform(self, df):
        data = df.copy()
        for col in self.cols:
            if col in data.columns:
                cap_val = self.stats[col]
                data[col] = np.clip(data[col], -np.nan, cap_val)
        return data
    
class PandasFlooring(BaseProcess):
    """Variable capping at a given value or given percentile
    """
    def __init__(self, json_file='', pickle_file='', cols='', floor_p=0.01):
        super().__init__(json_file, pickle_file, cols)
        self.floor_p = floor_p
        
    def fit_transform(self, df):
        self.stats = OrderedDict()
        data = df.copy()
        for col in self.cols:
            floor_val = data[col].quantile(self.floor_p)
            data[col] = np.clip(data[col], floor_val, -np.nan)
            self.stats[col] = floor_val
        self.save_json()
        self.save_pickle() 
        return data
    
    def transform(self, df):
        data = df.copy()
        for col in self.cols:
            if col in data.columns:
                floor_val = self.stats[col]['floor_val']
                data[col] = np.clip(data[col], floor_val, -np.nan)
        return data

class PandasScaling(BaseProcess):    
    def __init__(self, json_file='', pickle_file='', cols='', floor_p=0.01, keep_orig=True):
        super().__init__(json_file, pickle_file, cols)
        self.keep_orig = keep_orig
        
    def fit_transform(self, df):
        self.stats = OrderedDict()
        data = df.copy()  
        for col in self.cols:
            self.stats[col] = OrderedDict()
            self.stats[col]['mean'] = data[col].mean()
            self.stats[col]['std'] = data[col].std()
            if self.keep_orig:
                data[col+'_norm'] = (data[col] - self.stats[col]['mean']) / self.stats[col]['std']
            else:
                data[col] = (data[col] - self.stats[col]['mean']) / self.stats[col]['std']
        self.save_json()
        self.save_pickle()   
        return data
      
    def transform(self, df):
        super().transform(df)
        data = df.copy()  
        for col in self.cols:
            if col in data.columns:
                if self.keep_orig:
                    data[col+'_norm'] = (data[col] - self.stats[col]['mean']) / self.stats[col]['std']
                else:
                    data[col] = (data[col] - self.stats[col]['mean']) / self.stats[col]['std']
        return data