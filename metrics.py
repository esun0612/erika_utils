#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:44 2019

@author: esun

Utility functions for metrics
"""
import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype
from sklearn import metrics
from util_data import eread, emerge
try:
    from util_data import map_cat_to_num
except:
    pass

def auc_to_gini(auc):
    """Convert ROC AUC to gini"""
    return 2 * auc - 1

def _unscaled_gini(true, pred):
    """Unscaled Gini calculation for continuous variables"""
    df = pd.DataFrame({'true':true, 'pred':pred})
    true_min = df['true'].min()
    if true_min < 0:
        df['true'] = df['true'] - true_min 
        df['pred'] = df['pred'] - true_min 
    total = df['true'].sum()
    y = df.sort_values('pred', ascending=False)['true'].cumsum().values / total
    trap = (2 * y.sum() - y[-1]) / (2 * df.shape[0])
    return trap - 1/2

def gini(true, pred):
    """Compute model gini"""
    try:
        return auc_to_gini(metrics.roc_auc_score(y_true=true, y_score=pred))
    except ValueError:
        return _unscaled_gini(true, pred) / _unscaled_gini(true, true)

def calculate_gini(df_orig, true_col, pred_col, dropna=False):
    """Calculate gini for categorical variable and also consider missing values
    """
    df = df_orig.copy()
    if not is_numeric_dtype(df[pred_col]):
        df[pred_col + '_num'] = map_cat_to_num(df, true_col, pred_col, change_df_orig=False)
        pred_col = pred_col + '_num'
    if not dropna:
        df[pred_col].fillna(-99999, inplace=True)
    else:
        df = df.dropna(subset=[pred_col])
    return gini(df[true_col], df[pred_col])

def accuracy_binary(true, pred):
    """Evaluate accuracy for classification problem
    """
    df = pd.DataFrame({'true': true, 'pred': pred})
    return 1 - np.abs(df['pred'] - df['true']).sum() / df.shape[0]    

def accuracy_multiclass(df, true_cols, pred_cols):
    """Evaluate accuracy for classification problem
    """
    return 1 - np.abs(df[true_cols].values - df[pred_cols].values).sum(axis=1).sum() / (2*df.shape[0])    
    
def accuracy_index(true, pred, wgt=None, bins=10, cont=True):
    """
    Calculate the accuracy index with weight
    If wgt is not specified, default wgt is set to one, i.e. no weight
    cont is the indicator for continuous or discrete
    """
    df = pd.DataFrame({'true': true, 'pred': pred})
    if wgt is None:
        df['wgt'] = 1
    else:
        df['wgt'] = wgt
    df = df.sort_values(by='pred', ascending=False)
    df['true'] = df['true'] * df['wgt']
    df['pred'] = df['pred'] * df['wgt']      
    if cont:
        try:
            accuracy = 1 - np.abs(df['pred'] - df['true']).sum() / np.abs(df['true']).sum()
            return accuracy
        except ZeroDivisionError:
            return np.nan
    else:
        df['bin'] = np.floor(df['wgt'].cumsum() / df['wgt'].sum() * bins) + 1
        df['bin'] = np.where(df['bin'] > bins, bins, df['bin'])
        df_agg = df.groupby('bin').agg({'bin': 'count', 'true': 'sum', 'pred': 'sum'})
        try:
            accuracy = 1 - np.abs(df_agg['pred'] - df_agg['true']).sum() / np.abs(df_agg['true']).sum()
            return accuracy
        except ZeroDivisionError:
            return np.nan

def key_financial_metrics(df, sign_col, rate_col, term_col, term_prob_cols, adj_rate_cols, tier_col, loan_amt_col='requested_amount'):
    """Key performance metrics
    """
    conv = df[sign_col].sum() / df.shape[0]
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
    total_signed_loan_amt = df['signed_loan_amt'].sum()
    volume_w_rate = (df[rate_col] * df['signed_loan_amt']).sum()
    wac =  volume_w_rate / total_signed_loan_amt
    
    #wac by each term 
    signed_loan_amt_by_term = np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values
    wac_by_term = (df[adj_rate_cols].fillna(0).values * signed_loan_amt_by_term).sum(axis=0) / signed_loan_amt_by_term.sum(axis=0)
    term_dist = signed_loan_amt_by_term.sum(axis=0) / total_signed_loan_amt
    
    #wac by each tier
    wac_by_tier = []
    #Signed loan amount tier distribution
    tier_dist = []
    for tier in range(1, 8):
        df_tier = df.loc[df[tier_col] == tier]
        wac_by_tier.append((df_tier[rate_col] * df_tier['signed_loan_amt']).sum() / df_tier['signed_loan_amt'].sum())
        tier_dist.append(df_tier['signed_loan_amt'].sum() / total_signed_loan_amt)
    
    result = [conv, wac] + list(term_dist) + tier_dist + list(wac_by_term) + wac_by_tier 
    return result

def performance_metrics(df, sign_col, rate_col, term_col, term_prob_cols, adj_rate_cols,
                        tier_col='tier', fico_col='credit_score', income_col='gross_income', loan_amt_col='requested_amount'):
    """Output performance metrics
    
    Inputs:
        sign_col: sign indicator
        rate_col: selected apr
        term_col: selected term
        term_prob_cols: list of column names for probability of each term
        adj_rate_cols: rate columns
    """
    population = df.shape[0]
    total_signed = df[sign_col].sum()
    pct_signed = total_signed / population
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
    total_signed_loan_amt = df['signed_loan_amt'].sum()
    conv_dollar_basis = total_signed_loan_amt / df[loan_amt_col].sum()
    volume_w_rate = (df[rate_col] * df['signed_loan_amt']).sum()
    wac =  volume_w_rate / total_signed_loan_amt
    wam = (df[term_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    avg_loan_amt = df['signed_loan_amt'].sum() / total_signed
    w_avg_fico = (df[fico_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    w_avg_income = (df[income_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    
    #wac by each term 
    signed_loan_amt_by_term = np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values
    wac_by_term = (df[adj_rate_cols].fillna(0).values * signed_loan_amt_by_term).sum(axis=0) / signed_loan_amt_by_term.sum(axis=0)
    term_dist = signed_loan_amt_by_term.sum(axis=0) / total_signed_loan_amt
    
    #wac by each tier
    wac_by_tier = []
    #Signed loan amount tier distribution
    tier_dist = []
    for tier in range(1, 8):
        df_tier = df.loc[df[tier_col] == tier]
        wac_by_tier.append((df_tier[rate_col] * df_tier['signed_loan_amt']).sum() / df_tier['signed_loan_amt'].sum())
        tier_dist.append(df_tier['signed_loan_amt'].sum() / total_signed_loan_amt)
    
    result = [population, total_signed, pct_signed, conv_dollar_basis, wac, wam, avg_loan_amt, w_avg_fico, w_avg_income] \
             + list(term_dist) + list(wac_by_term) + wac_by_tier + tier_dist
    return result

def tier_term_dist(df, sign_col, term_prob_cols, rate_cols, tier_col='tier', loan_amt_col='requested_amount'):
    """Calculate tier term distribution
    """
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
    signed_loan_amt_by_term = pd.DataFrame(np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values)
    signed_loan_amt_term_cols = [f'signed_loan_amt_{term}' for term in range(2,8)]
    signed_loan_amt_by_term.columns = signed_loan_amt_term_cols
    df = pd.concat([df, signed_loan_amt_by_term], axis=1)
    result = df.groupby(tier_col)[signed_loan_amt_term_cols].sum() / df['signed_loan_amt'].sum()
    return result

def eval_model_perform_term(df_orig, pred_term_prob_cols=[f'pred_term_{term}' for term in range(2, 8)], 
                            target_term_prob_cols=[f'initial_term_{term}' for term in range(2, 8)], target_term_col='initial_term', return_gini_all=False):
    """Evaluate average Gini and accuracy of term selection model
    """
    df = df_orig.copy()
    df = df.loc[df[target_term_col].notnull()]
    gini_all = []
    for pred_term_prob_col, target_term_prob_col in zip(pred_term_prob_cols, target_term_prob_cols):
        gini_all.append(gini(df[target_term_prob_col], df[pred_term_prob_col]))
    gini_result = sum(gini_all) / len(gini_all)
    term_dist = (df[target_term_col].value_counts().sort_index() / df.shape[0]).values
    wgt_gini = (term_dist * np.array(gini_all)).sum()
    accuracy_result = accuracy_multiclass(df, target_term_prob_cols, pred_term_prob_cols)
    if not return_gini_all:
        return [gini_result, accuracy_result, wgt_gini]
    else:
        return [gini_result, accuracy_result, wgt_gini] + gini_all

def evaluate_financial_metrics_diff(df, rate_cols, act_term_prob_cols, pred_term_prob_cols, terms, rate_pred_col, loss_cols=[f'loss_{term}' for term in range(2,8)]):
    """Calculate financial metrics difference between actual and prediction
    """
    fm_actual = np.array(performance_metrics(df, 'signed_ind', 'interest_rate', 'initial_term', act_term_prob_cols, rate_cols, 'actual_loss', tier_col='tier')[2:])
    df['avg_rate'] = np.sum(df[rate_cols].fillna(0).values * df[pred_term_prob_cols].values, axis=1)
    df['avg_maturity'] = np.sum(np.array(terms) * df[pred_term_prob_cols].values, axis=1)
    df['avg_loss'] = np.sum(df[loss_cols].fillna(0).values * df[pred_term_prob_cols].values, axis=1)
    fm_pred = np.array(performance_metrics(df, rate_pred_col, 'avg_rate', 'avg_maturity', pred_term_prob_cols, rate_cols, 'avg_loss', tier_col='tier_pre')[2:])
    fm_diff = fm_pred - fm_actual
    fm_diff = pd.DataFrame(fm_diff).transpose()
    cols = ['pct_signed', 'conv_dollar_basis', 'wac', 'waloss', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income'] \
            + ['term_dist_{}'.format(term) for term in terms] + ['wac_term_{}'.format(term) for term in terms] \
            + ['wac_tier_{}'.format(tier) for tier in range(1,8)] + ['tier_dist_{}'.format(tier) for tier in range(1,8)]
    fm_diff.columns = cols 
    fm_diff['term_dist_diff_sum'] = fm_diff[[f'term_dist_{term}' for term in terms]].abs().sum(axis=1)
    fm_diff['wac_term_diff_sum'] = fm_diff[[f'wac_term_{term}' for term in terms]].abs().sum(axis=1)
    fm_diff['wac_tier_diff_sum'] = fm_diff[[f'wac_tier_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    fm_diff['tier_dist_diff_sum'] = fm_diff[[f'tier_dist_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    selected_cols = ['pct_signed', 'conv_dollar_basis', 'wac', 'waloss', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income'] \
                    + [f'term_dist_{term}' for term in terms] + ['term_dist_diff_sum'] \
                    + [f'wac_term_{term}' for term in terms] + ['wac_term_diff_sum'] \
                    + [f'wac_tier_{tier}' for tier in range(1,8)] + ['wac_tier_diff_sum'] \
                    + [f'tier_dist_{tier}' for tier in range(1,8)] + ['tier_dist_diff_sum'] 
    fm_diff = fm_diff[selected_cols]
    return fm_diff
    
def evaluate_financial_metrics_wrapper(data_file, rate_pred_file, term_pred_file, rate_pred_col, term_pred_cols, rate_cols=[f'min_rate_{term}' for term in range(2, 8)]):
    """Wrapper function for financial metrics evaluation
    """
    terms = range(2, 8)
    df = eread(data_file)
    n = df.shape[0]
    act_term_prob_cols = [f'signed_ind_{term}' for term in terms]
    pred_term_prob_cols = term_pred_cols
    df = df[['id', 'tier_pre', 'requested_amount', 'credit_score', 'gross_income', 'tier', 'interest_rate', 'actual_loss'] + [f'loss_{term}' for term in terms] 
            + act_term_prob_cols]
    rate_pred = eread(rate_pred_file)
    term_pred = eread(term_pred_file)
    df = emerge([df, rate_pred, term_pred], 'id')
    assert df.shape[0] == n, "Merge not perfect, check data!"
    return evaluate_financial_metrics_diff(df, rate_cols, act_term_prob_cols, pred_term_prob_cols, terms, rate_pred_col)