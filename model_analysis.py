#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 12:37:20 2019

@author: esun
"""

from os.path import join as pjoin
import glob
import pandas as pd

def collect_result(model_output_dir, exps, data_name, how, output=True, output_name=None, terms=range(2, 8)):
    """Collect result in model_output_dir 
    
    Input:
        exps: list of experiment numbers
        data_name: oot / oos
        how: 'absolute' or 'ratio'
    """
    assert how in ['absolute', 'ratio'], "Please select how from ['absolute', 'ratio']!"
    if how == 'absolute':
        diff_method = 'diff'
    elif how == 'ratio':
        diff_method = 'diff_ratio'
    model_dirs = [glob.glob(pjoin(model_output_dir, f'{exp}*'))[0] for exp in exps]
    final_result = []
    for model_dir in model_dirs:
        rate_perf_file = glob.glob(pjoin(model_dir, 'metrics_rate_*'))[0]
        term_perf_file = glob.glob(pjoin(model_dir, 'metrics_term_*'))[0]
        final_perf_file = glob.glob(pjoin(model_dir, 'final_metrics_*'))[0]
        rate_perf = pd.read_csv(pjoin(model_dir, rate_perf_file), index_col=0)
        term_perf = pd.read_csv(pjoin(model_dir, term_perf_file), index_col=0)
        final_perf = pd.read_csv(pjoin(model_dir, final_perf_file), index_col=0)
        final_perf = final_perf.iloc[:, 2:]
        final_result.append(list(rate_perf.loc[data_name].values) + list(term_perf.loc[data_name].values) \
                            + list(final_perf.loc[f'{data_name}_{diff_method}'].values))
    df_result = pd.DataFrame(final_result) 
    orig_cols = [f'{metric}_rate' for metric in ['gini', 'acc', 'auc']] + [f'{metric}_term' for metric in ['gini', 'acc', 'auc']] + final_perf.columns.to_list()
    df_result.columns = orig_cols
    df_result['term_dist_diff_sum'] = df_result[[f'term_dist_{term}' for term in terms]].abs().sum(axis=1)
    df_result['wac_term_diff_sum'] = df_result[[f'wac_term_{term}' for term in terms]].abs().sum(axis=1)
    df_result['wac_tier_diff_sum'] = df_result[[f'wac_tier_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    df_result['tier_dist_diff_sum'] = df_result[[f'tier_dist_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    orig_cols.insert(orig_cols.index('wac_term_2'), 'term_dist_diff_sum')
    orig_cols.insert(orig_cols.index('wac_tier_1'), 'wac_term_diff_sum')
    orig_cols.insert(orig_cols.index('tier_dist_1'), 'wac_tier_diff_sum')
    orig_cols.append('tier_dist_diff_sum')
    df_result = df_result[orig_cols]
    df_result.index = exps
    if output:
        if output_name is None:
            output_name = f'result_exp_{exps[0]}_{exps[-1]}_{data_name}.csv'
        df_result.to_csv(pjoin(model_output_dir, output_name))
    return df_result

def collect_result_2(model_output_dir, exps, data_name, how, output=True, output_name=None, terms=range(2, 8)):
    """Collect result in model_output_dir 
    
    Input:
        exps: list of experiment numbers
        data_name: oot / oos
        how: 'absolute' or 'ratio'
    """
    assert how in ['absolute', 'ratio'], "Please select how from ['absolute', 'ratio']!"
    if how == 'absolute':
        diff_method = 'diff'
    elif how == 'ratio':
        diff_method = 'diff_ratio'
    model_dirs = [glob.glob(pjoin(model_output_dir, f'{exp}*'))[0] for exp in exps]
    final_result = []
    for model_dir in model_dirs:
        final_perf_file = glob.glob(pjoin(model_dir, 'final_metrics_*'))[0]
        final_perf = pd.read_csv(pjoin(model_dir, final_perf_file), index_col=0)
        final_perf = final_perf.iloc[:, 2:]
        final_result.append(list(final_perf.loc[f'{data_name}_{diff_method}'].values))
    df_result = pd.DataFrame(final_result) 
    orig_cols = final_perf.columns.to_list()
    df_result.columns = orig_cols
    df_result['term_dist_diff_sum'] = df_result[[f'term_dist_{term}' for term in terms]].abs().sum(axis=1)
    df_result['wac_term_diff_sum'] = df_result[[f'wac_term_{term}' for term in terms]].abs().sum(axis=1)
    df_result['wac_tier_diff_sum'] = df_result[[f'wac_tier_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    df_result['tier_dist_diff_sum'] = df_result[[f'tier_dist_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    orig_cols.insert(orig_cols.index('wac_term_2'), 'term_dist_diff_sum')
    orig_cols.insert(orig_cols.index('wac_tier_1'), 'wac_term_diff_sum')
    orig_cols.insert(orig_cols.index('tier_dist_1'), 'wac_tier_diff_sum')
    orig_cols.append('tier_dist_diff_sum')
    df_result = df_result[orig_cols]
    df_result.index = exps
    if output:
        if output_name is None:
            if len(exps) > 1:
                output_name = f'result_exp_{exps[0]}_{exps[-1]}_{data_name}.csv'
            else:
                output_name = f'result_exp_{exps[0]}_{data_name}.csv'
        df_result.to_csv(pjoin(model_output_dir, output_name))
    return df_result

def merge_var_importance(var_imp_file_1, var_imp_file_2, output_file):
    """Merge two variable importance file
    """
    df1 = pd.read_csv(var_imp_file_1)
    df2 = pd.read_csv(var_imp_file_2)
    df = pd.merge(df1, df2, on='Variable', how='outer')
    df.sort_values(by='Rel_imp_gain_y', ascending=False, inplace=True)
    df.to_csv(output_file, index=None)
    