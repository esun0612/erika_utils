#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 12:15:53 2019

@author: esun
"""
import os
import lightgbm as lgb
from math import fabs as fabs
import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import impute_rate, eread, create_fico_loan_bin
from util_model import Header, object2cat 

def gen_rank_plot_dat(df, dep_var_cols, indep_var_col, multi_class=False, 
                      pct_groups=10, cat_groups=10, missing_pct=0.05, discre_num_pct=0.01):
    """Generate the data for rank and plot
    
    Input:
        df: DataFrame
        dep_var_cols: column names of dependent variable(s)
        indep_var_col: column name of independent variable
        pct_groups: number of groups for continuous variable
        cat_groups: number of distinct values to be considered as discrete numerical variable
        missing_pct: percentage to categorize missing as a unique group and show in the plot
        discre_num_pct: in the plot for discrete numerical variable, discard categories if percentage < discre_num_pct
    """
    if not isinstance(dep_var_cols, list) and not multi_class:
        dep_var_cols = [dep_var_cols]
    #Categorical variable
    if not is_numeric_dtype(df[indep_var_col]):
        if not multi_class:
            stat = df.groupby(indep_var_col)[dep_var_cols].mean()
        else:
            stat = df.groupby(['group_var', dep_var_cols])[dep_var_cols].count() / df.groupby('group_var')[dep_var_cols].count()
            stat.name = 'cnt'
            stat = pd.DataFrame(stat).reset_index()
            stat = pd.pivot_table(stat, index='group_var', columns=dep_var_cols, values='cnt')
            stat_dep_var_cols = stat.columns
            dep_var_stats = [stat[dep_var].tolist() for dep_var in stat_dep_var_cols]
        stat.index = stat.index.str[:15]
        var_cnt = cat_cnt(df[indep_var_col], missing_pct=missing_pct)
        stat = pd.merge(var_cnt, stat, left_index=True, right_index=True, how='left')
        if 'Rest' in stat.index:
            if not multi_class:
                stat.loc['Rest', dep_var_cols] = df[(~df[indep_var_col].isin(var_cnt['orig_index'])) & (df[indep_var_col].notnull())][dep_var_cols].mean()
            else:
                df_rest = df[(~df[indep_var_col].isin(var_cnt['orig_index'])) & (df[indep_var_col].notnull())]
                stat_missing = df_rest[dep_var_cols].value_counts().sort_index() / df_rest[dep_var_cols].count()
                for col in stat_dep_var_cols:
                    stat.loc['Rest', col] = stat_missing.loc[col]
        if 'Missing' in stat.index:
            if not multi_class:
                stat.loc['Missing', dep_var_cols] = df[df[indep_var_col].isnull()][dep_var_cols].mean()
            else:
                stat_missing = df[df[indep_var_col].isnull()][dep_var_cols].value_counts().sort_index() / df[df[indep_var_col].isnull()][dep_var_cols].count()
                for col in stat_dep_var_cols:
                    stat.loc['Missing', col] = stat_missing.loc[col]
        return [str(i) for i in range(0, len(stat))], stat.iloc[:, -len(dep_var_cols):]
    
    #Numerical variable
    else:
        #Numerical variable with unique values less than cat_groups
        if df[indep_var_col].nunique() <= cat_groups:
            if not multi_class:
                dep_var_stats = df.groupby(indep_var_col)[dep_var_cols].mean()
            else:
                dep_var_stats = df.groupby([indep_var_col, dep_var_cols])[dep_var_cols].count() / df.groupby(indep_var_col)[dep_var_cols].count()
                dep_var_stats = pd.DataFrame(stat).reset_index()
                dep_var_stats = pd.pivot_table(stat, index=indep_var_col, columns=dep_var_cols, values=dep_var_cols)
            out_x_orig = list(dep_var_stats.index)
            var_cnt = df[indep_var_col].value_counts() / df.shape[0]
            out_x_orig = [idx for idx in out_x_orig if var_cnt[idx] > 0.01]
            out_x = ["{}: {:.1%}".format(val, var_cnt[val]) for val in out_x_orig]
            dep_var_stats = dep_var_stats.loc[out_x_orig]
        else:
            x = [df[indep_var_col].min()] + df[indep_var_col].quantile([i / pct_groups for i in range(1,pct_groups)], interpolation='nearest').tolist() + [df[indep_var_col].max()]
            uni_x = sorted(list(set(x)))
            df['group_var'] = pd.cut(df[indep_var_col], uni_x, include_lowest=True)
            out_x = df.groupby('group_var')[indep_var_col].median().tolist()
            if not multi_class:
                stat = df.groupby('group_var')[dep_var_cols].mean()
                dep_var_stats = [stat[dep_var].tolist() for dep_var in dep_var_cols]
            else:
                stat = df.groupby(['group_var', dep_var_cols])[dep_var_cols].count() / df.groupby('group_var')[dep_var_cols].count()
                stat.name = 'cnt'
                stat = pd.DataFrame(stat).reset_index()
                stat = pd.pivot_table(stat, index='group_var', columns=dep_var_cols, values='cnt')
                stat_dep_var_cols = stat.columns
                dep_var_stats = [stat[dep_var].tolist() for dep_var in stat_dep_var_cols]
            for i, x_value in enumerate(x[:-1]):
                if x[i + 1] == x[i]:
                    out_x.insert(i, x[i])
                    if not multi_class:
                        for y, dep_var in zip(dep_var_stats, dep_var_cols):  
                            y.insert(i, df[df[indep_var_col] == x[i]][dep_var].mean())
                    else:
                        for y, dep_var_value in zip(dep_var_stats, stat_dep_var_cols.values):
                            y.insert(i, df[(df[indep_var_col] == x[i]) & (df[dep_var_cols] == dep_var_value)][dep_var_cols].count() / df[df[indep_var_col] == x[i]][dep_var_cols].count())
                        
            dep_var_stats = pd.DataFrame(dep_var_stats).transpose()
            dep_var_stats.columns = dep_var_cols if not multi_class else stat_dep_var_cols
        
        #Include missing statistics if missing percentage > missing_pct
        indep_var_col_missing_cpt = df[indep_var_col].isnull().sum() / df.shape[0]
        if indep_var_col_missing_cpt > missing_pct:
            out_x.append('M: {0:.1%}'.format(indep_var_col_missing_cpt))
            if not multi_class:
                dep_var_stats = dep_var_stats.append(df[df[indep_var_col].isnull()][dep_var_cols].mean(), ignore_index = True) 
            else:
                dep_var_stats = dep_var_stats.append(df[df[indep_var_col].isnull()][dep_var_cols].value_counts().sort_index() / df[df[indep_var_col].isnull()][dep_var_cols].count(), 
                                                    ignore_index = True)
        return out_x, dep_var_stats

def cust_format_time(x):
    """format x for rank and plots
    """
    if x < 60:
        return "{:.1f}s".format(x)
    elif x < 3600:
        return "{0:.0f}m:{1:.0f}s".format(x // 60, x % 60)
    elif x < 86400:
        return "{0:.0f}h:{1:.0f}m".format(x // 3600, x%3600 // 60)
    else:
        return "{0:.0f}d{1:.0f}h:{2:.0f}m".format(x // 2073600, x%2073600 // 3600, x%2073600%3600 // 60)
    
def cust_format(x):
    """format x for rank and plots
    """
    if isinstance(x, str):
        return x
    if x == 0:
        return "{0:.1f}".format(x)
    elif fabs(x) < 0.001:
        return "{0:.2%}".format(x)
    elif fabs(x) < 1:
        return "{0:.1%}".format(x)
    elif fabs(x) < 5:
        return "{0:.1f}".format(x)
    elif fabs(x) < 1000:
        return "{0:.0f}".format(x)
    elif fabs(x) < 1e6:
        return "{0:.0f}K".format(x / 1000)
    else:
        return "{0:.0f}M".format(x / 1e6)
    
def rank_plot(x, y, ax=None, title=None, total_x_ticks=10, title_fontsize=8, tick_fontsize=4):
    """
    Plot the x, y in bars
    Inputs: x and y are the outputs from gen_rank_plot_dat
    total_x_ticks are the total number of ticks on x-axis
    """
    n = len(x)
    if ax is None:
        fig, ax = plt.subplots()
    try:
        ax.bar(range(1, n + 1), y, width = 0.4, align='edge', color='cadetblue')
    except TypeError:
        ax.bar(range(1, n + 1), y.iloc[:, 0], width = 0.4, align='edge', color='cadetblue')
    ax.set_xlim(0.7, n + 0.8)
    #Customize the x ticks based on x values
    x_ticks_step = max(1, n // total_x_ticks)
    cust_x_labels = [cust_format(item) for item in x[::x_ticks_step]] 
    ax.set_xticks(np.arange(1.2, n + 1, x_ticks_step))
    ax.set_xticklabels(cust_x_labels)
    ax.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    if title is not None:
        ax.set_title(title, fontsize=title_fontsize)

def rank_plot_multilabel(x, y, ax=None, title=None, total_x_ticks=10, title_fontsize=8, tick_fontsize=4, legend_fontsize=4):
    """
    Plot the x, y in bars for multilables
    Inputs: x and y are the outputs from gen_rank_plot_dat_multilabel
    total_x_ticks are the total number of ticks on x-axis
    """
    n = len(x)
    if ax is None:
        fig, ax = plt.subplots()
    plots = []
    for idx, col in enumerate(y.columns):
        if idx == 0:
            p = ax.bar(range(1, n + 1), y[col].values, width = 0.4, align='edge')
        else:
            p = ax.bar(range(1, n + 1), y[col], bottom=y.iloc[:, :idx].sum(axis=1), width = 0.4, align='edge')
        plots.append(p)
    ax.set_xlim(0.7, n + 0.8)
    #Customize the x ticks based on x values
    x_ticks_step = max(1, n // total_x_ticks)
    cust_x_labels = [cust_format(item) for item in x[::x_ticks_step]] 
    ax.set_xticks(np.arange(1.2, n + 1, x_ticks_step))
    ax.set_xticklabels(cust_x_labels)
    ax.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    ax.legend((p[0] for p in plots), tuple(list(y.columns)), loc=8, ncol=3, bbox_to_anchor=(0.5, -0.3), fontsize=legend_fontsize)
    if title is not None:
        ax.set_title(title, fontsize=title_fontsize)

def barh_plot(x, labels, y_ticks, ax=None, tick_fontsize=4, label_size=4):
    """Create horizontal bar plot with data x, number lable of bar labels and y_ticks
    """
    if ax is None:
        fig, ax = plt.subplots()
    n = len(x)
    rects = ax.barh(range(n, 0, -1), x)
    ax.set_yticks(range(n, 0, -1))
    ax.set_yticklabels(y_ticks)
    ax.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    
    # For each bar: Place a label
    for rect, label in zip(rects, labels):
        # Get X and Y placement of label from rect.
        x_value = rect.get_width()
        y_value = rect.get_y() + rect.get_height() / 2
        # Number of points between bar and label. Change to your liking.
        space = 5
        # Vertical alignment for positive values
        ha = 'left'
        # If value of bar is negative: Place label left of bar
        if x_value < 0:
            # Invert space to place label to the left
            space *= -1
            # Horizontally align label at right
            ha = 'right'
        #label = "{:.1f}".format(x_value)
    
        # Create annotation
        ax.annotate(
            label,                      # Use `label` as label
            (x_value, y_value),         # Place label at end of the bar
            xytext=(space, 0),          # Horizontally shift label by `space`
            textcoords="offset points", # Interpret `xytext` as offset in points
            va='center',                # Vertically center label
            ha=ha,                      # Horizontally align label differently for positive and negative values.
            fontsize=label_size)                      

def cat_cnt(x, missing_pct=0.05):
    """Generate categorical counts for a given categorical series x
    """      
    var_cnt_raw = x.value_counts()
    var_cnt = pd.DataFrame(var_cnt_raw[:10]) if len(var_cnt_raw) >= 10 else pd.DataFrame(var_cnt_raw)
    var_cnt.columns = ['cnt']
    if len(var_cnt_raw) >= 10:
        var_cnt.loc['Rest'] = var_cnt_raw[10:].sum()
    var_missing_cnt = x.isnull().sum()
    if var_missing_cnt / len(x) >= missing_pct:
        var_cnt.loc['Missing'] =var_missing_cnt
    var_cnt['label'] = var_cnt / var_cnt['cnt'].sum()
    var_cnt['label'] = var_cnt['label'].map('{0:.1%}'.format)
    var_cnt = var_cnt.reset_index()
    var_cnt['orig_index'] = var_cnt['index']
    var_cnt['index'] = var_cnt['orig_index'].str[:15]
    var_cnt = var_cnt.set_index('index', drop=False)
    var_cnt['index_label'] = [f'{i}: {index}' for i, index in enumerate(var_cnt.index)]
    var_cnt = var_cnt[['cnt', 'label', 'index_label', 'orig_index']]
    return var_cnt

def plot_hist(x, num_plot_sample=100000, cat_groups=10, ax=None, clip=True, 
              bins=50, title=None, title_fontsize=8, tick_fontsize=4, hist_cat_tick_fontsize=3):
    """Generate histogram of a given column x
    """
    if ax is None:
        fig, ax = plt.subplots()
    if is_numeric_dtype(x):
        if len(x) > num_plot_sample:
            x = x.sample(n=num_plot_sample)
        if clip:
            x = np.clip(x, x.quantile(0.01), x.quantile(0.99))
        ax.hist(x.dropna(), bins=bins)
        ax.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    else:
        var_cnt = cat_cnt(x)
        barh_plot(var_cnt['cnt'], var_cnt['label'], var_cnt['index_label'], ax, tick_fontsize=hist_cat_tick_fontsize)
    if title is not None:
        ax.set_title(title, fontsize=title_fontsize)
        
def gen_analysis_plot(df, target_col, multilabel_target_cols, indep_var_col, multi_class_target_col=None,
                      title=None, title_fontsize=6, multilabel_title='term', tick_fontsize=4):
    """Generate the analysis plot including histogram, rank plot for target_col and multilabel_target_cols
    """
    n = df.shape[0]
    #Figure title
    if title is None:
        title = indep_var_col
    fig, axs = plt.subplots(2, 2)
    plt.subplots_adjust(wspace=0.2, hspace=0.3)
    
    #Histogram
    if is_numeric_dtype(df[indep_var_col]):
        plot_var_missing_rt = df[indep_var_col].isnull().sum() / n
        plot_mean = df[indep_var_col].mean()
        histogram_title = 'histogram: missing rate: {0:.1%}, mean: {1:.1f}'.format(plot_var_missing_rt, plot_mean)
    else:
        histogram_title = 'histogram'
    plot_hist(df[indep_var_col], ax=axs[0][0], title=histogram_title, 
              title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Rank plot for target_col
    x, y = gen_rank_plot_dat(df, target_col, indep_var_col)
    rank_plot(x, y, ax=axs[0][1], title=f'rank plots with {target_col}', 
              title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Rank plot for multilabel_target_cols
    xm, ym = gen_rank_plot_dat(df, multilabel_target_cols, indep_var_col)
    rank_plot_multilabel(xm, ym, axs[1][0], title=f'rank plots with {multilabel_title } {target_col}', 
                         title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Rank plot for target_col more granular bins if multi_class_target_col is None, else distribution for term selection variable
    if multi_class_target_col is None:
        x, y = gen_rank_plot_dat(df, target_col, indep_var_col, pct_groups=50)
        rank_plot(x, y, ax=axs[1][1], title=f'rank plots with {target_col} detail', 
                  title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    else:
        xm, ym = gen_rank_plot_dat(df, multi_class_target_col, indep_var_col, multi_class=True)
        rank_plot_multilabel(xm, ym, axs[1][1], title=f'multi-class distribution with {multi_class_target_col}', 
                             title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    plt.suptitle(title, fontsize=8)
    return fig

def gen_analysis_plot_2(df, target_col, indep_var_col, multi_class_target_col,
                      title=None, title_fontsize=6, multilabel_title='term', tick_fontsize=4):
    """Generate the analysis plot including histogram, rank plot for target_col and distribution for multi_class_target_col
    """
    n = df.shape[0]
    #Figure title
    if title is None:
        title = indep_var_col
    fig, axs = plt.subplots(2, 2)
    plt.subplots_adjust(wspace=0.2, hspace=0.3)
    
    #Histogram
    if is_numeric_dtype(df[indep_var_col]):
        plot_var_missing_rt = df[indep_var_col].isnull().sum() / n
        plot_mean = df[indep_var_col].mean()
        histogram_title = 'histogram: missing rate: {0:.1%}, mean: {1:.1f}'.format(plot_var_missing_rt, plot_mean)
    else:
        histogram_title = 'histogram'
    plot_hist(df[indep_var_col], ax=axs[0][0], title=histogram_title, 
              title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Rank plot for target_col
    x, y = gen_rank_plot_dat(df, target_col, indep_var_col)
    rank_plot(x, y, ax=axs[0][1], title=f'rank plots with {target_col}', 
              title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Rank plot for target_col more granular bins
    x, y = gen_rank_plot_dat(df, target_col, indep_var_col, pct_groups=50)
    rank_plot(x, y, ax=axs[1][0], title=f'rank plots with {target_col} detail', 
                  title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)
    
    #Distribution for term selection variable
    xm, ym = gen_rank_plot_dat(df, multi_class_target_col, indep_var_col, multi_class=True)
    rank_plot_multilabel(xm, ym, axs[1][1], title=f'multi-class distribution with {multi_class_target_col}', 
                         title_fontsize=title_fontsize, tick_fontsize=tick_fontsize)

    plt.suptitle(title, fontsize=8)
    return fig

def gen_stat(df, var):
    """
    Output the statistics of *var in a df as a DataFrame
    """
    stat1 = df[var].describe()
    stat2 = df[var].quantile([0.01, 0.05, 0.10, 0.90, 0.95, 0.99])
    stat2.index = ['1%', '5%', '10%', '90%', '95%', '99%']
    stat = pd.concat([stat1, stat2])
    stat = stat.reindex(['count', 'mean', 'std', 'min', 
                         '1%', '5%', '10%', '25%', '50%', '75%', '90%', '95%', '99%', 'max'])
    if isinstance(var, list):
        return stat
    else:
        return pd.DataFrame(stat)

def compare_dat(df1, df2, idvar=None, common_cols=0, by_index=False):
    """
    For common_cols in df1 and df2, calculate the difference and output the corresponding min and max
    """
    if not by_index and idvar is None:
        print ("Please specify merge by colomuns or index!")
        return
    
    #Check number of observations
    if df1.shape[0] == df2.shape[0]:
        print ("Number of observations {} match!".format(df1.shape[0]))
    else:
        print ("Number of observations do NOT match!")
    
    if common_cols == 0:
        try:
            common_cols = list(set(df1.columns.tolist()) & set(df2.columns.tolist()) - set([idvar]))
        except TypeError:
            common_cols = list(set(df1.columns.tolist()) & set(df2.columns.tolist()) - set(idvar))
            
    compare = pd.merge(df1, df2, left_on=idvar, right_on=idvar, left_index=by_index, right_index=by_index)
    
    #Check number of observations after the merge
    if compare.shape[0] == df1.shape[0]:
        print ("Perfect merge!")
    else:
        print ("Merge is not complete, please check the merge condition!")
    
    #Check data types
    dtype1=pd.DataFrame(df1.dtypes)
    dtype2=pd.DataFrame(df1.dtypes)
    if dtype1.equals(dtype2):
        print ("Data type matches!")
    else:
        print ("Data type does NOT match!")
        
    
    for model_col in sorted(common_cols):
        col_x = '{}_x'.format(model_col)
        col_y = '{}_y'.format(model_col)
        col_diff = 'diff_{}'.format(model_col)
        try:
            compare[col_diff] = compare[col_y] - compare[col_x] 
            print (model_col, compare[col_diff].min(), compare[col_diff].max())
        except TypeError:
            print ("{} is not numerical type".format(model_col))
            print ("Match on two columns: {}".format(compare[col_x].equals(compare[col_y])))
        
    return compare
   
def compare_dat_2(df_orig, var1, var2, output_dir=None, out_nm=None, sharey=True):
    """
    Compare var 1 and var2 in a given df.
    Output the statistics and histograms of the difference and each variable to a output_dir with out_nm
    """
    df = df_orig.copy()
    df['diff'] = df[var2] - df[var1]
    stat_diff = gen_stat(df, 'diff')
    stat_var2 = gen_stat(df, var2)
    stat_var1 = gen_stat(df, var1)
    stat = pd.merge(stat_diff, stat_var1, left_index=True, right_index=True)
    stat = pd.merge(stat, stat_var2, left_index=True, right_index=True)
    stat.rename(columns={'diff': f'{var2} - {var1}'})
    stat['quantile_diff'] = stat[var2] - stat[var1]
    
    fig, axs = plt.subplots(1,3,figsize=(18,6), sharey=sharey)
    #Subplot can be called by axs[0], axs[1], axs[2]
    for (ax, var, title) in zip(axs, ['diff', var2, var1], [f'{var2} - {var1}', var2, var1]):
        ax.hist(df[var].dropna(), bins=50)
        ax.set_title(title)
        
    if output_dir is not None and out_nm is not None:
        fig.savefig(os.path.join(output_dir, '{}_histo.pdf'.format(out_nm)))   
        stat.to_csv(os.path.join(output_dir, '{}_stat.csv'.format(out_nm)))
    return stat, fig

def compare_dat_3(df_orig, var1, var2, output_dir=None, out_nm=None, sharey=True, return_diff_df=False):
    """
    Compare var 1 and var2 in a given df
    Output the shape where var1 and var2 are different
    Populate statistics on where they are different
    """
    df0 = df_orig.copy()
    n = df0.shape[0]
    if df0[df0[var2] != df0[var1]].shape[0] == 0:
        print (f"{var1} and {var2} are exactly the same!")
        return
    else:
        #Select only observations both vars are not null
        df = df0[(df0[var1].notnull()) & (df0[var2].notnull())]
        df_diff = df[df[var2] != df[var1]]
        diff_pct = df_diff.shape[0] / df.shape[0]
        var_1_null_cnt = df0[df0[var1].isnull()].shape[0]
        var_2_null_cnt = df0[df0[var2].isnull()].shape[0]
        print(f"Total observation is {n}")
        print("{0} / {1} = {2:.1%} observations are null for {3}".format(var_1_null_cnt, n, var_1_null_cnt / n, var1))
        print("{0} / {1} = {2:.1%} observations are null for {3}".format(var_2_null_cnt, n, var_2_null_cnt / n, var2))
        print("{0} / {1} = {2:.1%} observations are not null for both {3} and {4}".format(df.shape[0], n, df.shape[0] / n, var1, var2))
        print ("{0} / {1} = {2:.1%} observations are different when both vars are not null".format(df_diff.shape[0], df.shape[0], diff_pct))
            
        df_diff['diff'] = df_diff[var2] - df_diff[var1]
        stat_diff = gen_stat(df_diff, 'diff')
        stat_var2 = gen_stat(df_diff, var2)
        stat_var1 = gen_stat(df_diff, var1)
        stat = pd.merge(stat_diff, stat_var1, left_index=True, right_index=True)
        stat = pd.merge(stat, stat_var2, left_index=True, right_index=True)
        stat.rename(columns={'diff': f'{var2} - {var1}'})
        stat['quantile_diff'] = stat[var2] - stat[var1]
        
        fig, axs = plt.subplots(1,3,figsize=(18,6), sharey=sharey)
        #Subplot can be called by axs[0], axs[1], axs[2]
        for (ax, var, title) in zip(axs, ['diff', var2, var1], [f'{var2} - {var1}', var2, var1]):
            ax.hist(df_diff[var].dropna(), bins=50)
            ax.set_title(title)
            
        if output_dir is not None and out_nm is not None:
            fig.savefig(os.path.join(output_dir, '{}_histo.pdf'.format(out_nm)))   
            stat.to_csv(os.path.join(output_dir, '{}_stat.csv'.format(out_nm)))
        
        if not return_diff_df:
            return stat, fig
        else:
            return stat, fig, df_diff

def cal_psi(base_data, new_data, var, return_table=False):
    """Calculate PSI between base_data and new_data for given var
    """
    base_cut, bins = pd.qcut(base_data[var], 10, retbins=True, duplicates='drop')
    bins[0] = bins[0] - 0.001
    new_cut = pd.cut(new_data[var], bins=bins)
    base_cnt = base_cut.value_counts().sort_index() / base_data.shape[0]
    new_cnt = new_cut.value_counts().sort_index() / new_data.shape[0]
    all_cnt = pd.merge(base_cnt, new_cnt, left_index=True, right_index=True)
    all_cnt.columns = ['base_cnt', 'new_cnt']
    psi = ((all_cnt['new_cnt'] - all_cnt['base_cnt']) * np.log((all_cnt['new_cnt']/all_cnt['base_cnt']))).sum()
    if not return_table:
        return psi
    else:
        return psi, all_cnt

def gen_plot_conv_data(data_file, rate_model_file, model_header_file, impute_rate_dict=None, rate_lower_bd=-0.015, rate_higher_bd=0.015, rate_interval=0.0005):
    """Generate conversion plot data for lightgbm model
    """
    #1. Read data
    df = eread(data_file)
    header = Header(model_header_file)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    rate_cols_orig = [f'min_rate_{term}_orig' for term in range(2,8)]
    take_cols =  list((set(header.num_cols + header.cat_cols) - set(rate_cols)).union({'id', 'requested_amount', 'credit_score', 'tier_pre', 'consolidated_channel_model'})) 
    df_rate = df[['id'] + rate_cols_orig]
    df_rate.columns = ['id'] + rate_cols
    df = df[take_cols]
    df = object2cat(df, use_cat_cols=header.cat_cols)
    rate_model = lgb.Booster(model_file=rate_model_file)
    
    #2. Generate conversion prediction by rate change
    rate_chgs = np.arange(rate_lower_bd, rate_higher_bd+rate_interval, rate_interval)
    for idx, rate_chg in enumerate(rate_chgs):
        df_rate_chg = df_rate.copy()
        df_rate_chg[rate_cols] = df_rate_chg[rate_cols] + rate_chg
        if impute_rate_dict is None:
            impute_rate_dict={'min_rate_2': 0.16677,
                             'min_rate_3': 0.16677,
                             'min_rate_4': 0.17436,
                             'min_rate_5': 0.18133,
                             'min_rate_6': 0.15313,
                             'min_rate_7': 0.16115}
        df_rate_chg, _ = impute_rate(df_rate_chg, rate_cols=rate_cols, impute_rate_dict=impute_rate_dict, keep_orig=False)
        df_score = pd.merge(df, df_rate_chg, on='id')
        df_score['min_all_rates'] = df_score[rate_cols].min(axis=1)
        X = df_score[header.num_cols + header.cat_cols]
        rate_pred = rate_model.predict(X)
        df[f'rate_pred_{idx}'] = rate_pred
    return df, rate_chgs

def gen_plot_conv_data_residual(data_file, lr_model_file, tier_col='tier_pre', rate_lower_bd=-0.015, rate_higher_bd=0.015, rate_interval=0.0005):
    """Generate conversion plot data for logistic regression + residual model
    """
    #1. Read data
    coef = pd.read_csv(lr_model_file)
    df = eread(data_file)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    #Define tier indicator variable
    for tier in range(2, 8):
        df[f'tier_{tier}'] = np.where(df[tier_col] == tier, 1, 0)
    df_rate_orig = df[rate_cols].copy()
        
    #2. Generate conversion prediction by rate change
    rate_chgs = np.arange(rate_lower_bd, rate_higher_bd+rate_interval, rate_interval)
    for idx, rate_chg in enumerate(rate_chgs):
        df[rate_cols] = df_rate_orig + rate_chg
        for term in range(2, 8):
            df[f'pred_rate_{term}'] = 1 / (1+np.exp(-((coef[f'term_{term}'].values[1:] * df[[f'min_rate_{term}', 'credit_score', 'requested_amount'] + 
                                                                       [f'tier_{tier}' for tier in range(2, 8)] + 
                                                                       ['combined_organic', 'MKT-DM', 'Rest', 'SEM']].values).sum(axis=1) + coef[f'term_{term}'].values[0])))
        df['rate_lr'] = df[[f'pred_rate_{term}' for term in range(2, 8)]].mean(axis=1)
        df[f'rate_pred_{idx}'] = np.clip(df['rate_lr'] + df['residual_pred'], 0, 1)
    return df, rate_chgs

def plot_conv_sensitivity(df, dimension, rate_chgs):
    fig, ax = plt.subplots()
    ax.set_title(dimension)
    dimension_values = sorted(df[dimension].unique())
    rate_pred_cols = [f'rate_pred_{idx}' for idx in range(0, len(rate_chgs))]
    for dimension_value in dimension_values:
        ax.plot(range(1, len(rate_chgs) + 1), df[df[dimension] == dimension_value][rate_pred_cols].mean(), label=dimension_value, linewidth=1)
    ax.legend(loc=8, ncol=len(dimension_values), bbox_to_anchor=(0.5, -0.15), fontsize=4)
    ax.set_xticks(range(0, len(rate_chgs), len(rate_chgs) // 8))
    ax.set_xticklabels(['{0:.2%}'.format(rate_chg) for rate_chg in rate_chgs[0:len(rate_chgs):len(rate_chgs) // 8]])
    ax.tick_params(axis='both', which='major', labelsize=6)
    return fig

def calculate_slope(df, dimension, rate_chgs, dimension_values=None):
    """Calculate slope for each dimension
    """
    if dimension_values is None:
        dimension_values = sorted(df[dimension].unique())
    slope = []
    for dimension_value in dimension_values:
        slope.append((df[df[dimension] == dimension_value]['rate_pred_1'].mean()- df[df[dimension] == dimension_value]['rate_pred_0'].mean()) / (rate_chgs[1]-rate_chgs[0]))
    return slope, dimension_values 

def plot_slope(slope, dimension_values, dimension):
    """Plot slope
    """
    fig, ax = plt.subplots()
    ax.set_title(dimension)
    ax.plot(range(0, len(slope)), slope, 'o-', linewidth=1, markersize=3)
    ax.tick_params(axis='both', which='major', labelsize=6)
    ax.set_xticks(range(0, len(slope)))
    ax.set_xticklabels(dimension_values)
    for x, y in zip(range(0, len(slope)), slope):
        label = "{:.2f}".format(y)
        plt.annotate(label, # this is the text
                     (x, y), # this is the point to label
                     textcoords="offset points", # how to position the text
                     xytext=(0, 10), # distance from text to points (x,y)
                     ha='center', # horizontal alignment can be left, right or center
                     fontsize=8)
    return fig

def plot_slope_wrap(df, rate_chgs, output_file):
    """Plot conversion slope
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_file)
    df = create_fico_loan_bin(df)
    dimensions = ['tier_pre', 'fico_bin', 'loan_amt_bin', 'consolidated_channel_model']
    dimension_values = [None, None, ['[5000, 20000]', '(20000, 50000]', '(50000, 100000]'], 
                        ['Affiliate', 'combined_organic', 'SEM', 'Rest', 'MKT-DM']]
    for dimension, dimension_value in zip(dimensions, dimension_values):
        slope, dimension_values = calculate_slope(df, dimension, rate_chgs, dimension_value)
        fig = plot_slope(slope, dimension_values, dimension)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 
    
def plot_conv_sensitivity_wrap(df, rate_chgs, output_file):
    """Plot conversion sensitivity
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_file)
    df = create_fico_loan_bin(df)
    for dimension in ['tier_pre', 'fico_bin', 'loan_amt_bin', 'consolidated_channel_model']:
        fig = plot_conv_sensitivity(df, dimension, rate_chgs)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 
    
