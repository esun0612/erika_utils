#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:59 2019

@author: esun

Utility functions for model
"""
import os
from os.path import join as pjoin
import re
import json
import pickle
import time
from collections import OrderedDict 
import numpy as np
import pandas as pd
import lightgbm as lgb
import matplotlib.pyplot as plt
from util_data import eread, esave

class Header():
    """Helper class for header file
    """
    def __init__(self, header_file=None, header=None):
        if header_file is not None:
            self.header = pd.read_csv(header_file)
        elif header is not None:
            self.header = header
        else:
            "Please input header dataframe or header_file"
            return 
        self.cols = list(self.header.columns)
        self.types = list(self.header.loc[0])
        self.index = list(range(0, len(self.cols)))
        self.num_cols = [x for x, y in zip(self.cols, self.types) if y == 'N']
        self.num_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'N']
        self.cat_cols = [x for x, y in zip(self.cols, self.types) if y == 'C']
        self.cat_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'C']
        self.key_cols = [x for x, y in zip(self.cols, self.types) if y == 'K']
        self.key_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'K']
        self.label_cols = [x for x, y in zip(self.cols, self.types) if y == 'L']
        self.label_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'L']
        self.ignore_cols = [x for x, y in zip(self.cols, self.types) if y == 'I']
        self.ignore_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'I']
        
def object2cat(X, use_cat_cols=None, return_cat_cols=False):
    """Cast categorical columns to type 'category'
    """
    if use_cat_cols is None:
        cat_cols = []
        for c in X.columns:
            col_type = X[c].dtype
            if col_type == 'object' or col_type.name == 'category':
                X[c] = X[c].astype('category')
                cat_cols.append(c)
    else:
        cat_cols = use_cat_cols
        for c in use_cat_cols:
            X[c] = X[c].astype('category')
    if return_cat_cols:
        return X, cat_cols
    else:
        return X

def read_data_from_header(header_file, data_file, return_y=True):
    """Based on data_file and header_file, return X(training variables) and y (label)
    """
    header = Header(header_file)
    df = eread(data_file)
    X = df[header.num_cols + header.cat_cols]
    if return_y:
        y = df[header.label_cols]
        return X, y
    else:
        return X

def read_prepare_data(header_file, raw_data_file, use_cat_cols=True, fillna_value=None, return_y=True):
    """Wrapper function to read data from header, fillna and cast categorical variables
    """
    if return_y:
        X, y = read_data_from_header(header_file, raw_data_file, return_y=return_y)
    else:
        X = read_data_from_header(header_file, raw_data_file, return_y=return_y)
    header = Header(header_file)
    if fillna_value is not None:
        X = X.fillna(fillna_value)
    if use_cat_cols:
        X = object2cat(X, use_cat_cols=header.cat_cols)
    else:
        X = object2cat(X)
    if return_y:
        return X, y
    else:
        return X

def clean_data(df, header):
    """Clean df according to header for scoring purpose
    """
    X = df[header.num_cols + header.cat_cols]
    X = object2cat(X, use_cat_cols=header.cat_cols)
    return X
    
def missing_impute(df, header_file):
    """Impute by missing values specified in header_file
    Will implement in the future
    """
    return

def get_feature_importance(booster):
    """Read feature importance to pandas DataFrame based on booster object (LightGBM Python API)
    """
    fi_df = pd.DataFrame({'Variable': booster.feature_name(),
                          'Abs_imp_gain': booster.feature_importance(importance_type='gain'),
                          'Abs_imp_split': booster.feature_importance(importance_type='split')})
    for imp_type in ['gain', 'split']:
        fi_df[f'Rel_imp_{imp_type}'] = fi_df[f'Abs_imp_{imp_type}'] / fi_df[f'Abs_imp_{imp_type}'].sum()
        fi_df[f'Rel_imp_{imp_type}'] = fi_df[f'Rel_imp_{imp_type}'].apply(lambda x: '{:.1%}'.format(x))
    fi_df.sort_values('Abs_imp_gain', ascending=False, inplace=True)
    return fi_df

def train_lightgbm(fit_params, classifier_params, X_train, y_train, X_oos, y_oos, output_folder, model_name=None,
                   monotone_decreasing_cols=None, monotone_increasing_cols=None):
    """Train lightgbm
    """
    #Evaluation datasets used in training early stopping
    fit_params['eval_set'] = [(X_oos, y_oos), (X_train, y_train)]
    
    #Define model_name
    if model_name is None:
        model_name = ""
    else:
        model_name = '_' + model_name
    
    #Define monotone features in model
    monotone_decreasing_cols = [] if monotone_decreasing_cols is None else monotone_decreasing_cols
    monotone_increasing_cols = [] if monotone_increasing_cols is None else monotone_increasing_cols
    if monotone_decreasing_cols != [] or monotone_increasing_cols != []:
        x_cols = list(X_train.columns)
        constrs = [0] * len(x_cols)
        for rate_col in monotone_decreasing_cols:
            rate_col_ind = x_cols.index(rate_col)
            constrs[rate_col_ind] = -1
        for rate_col in monotone_increasing_cols:
            rate_col_ind = x_cols.index(rate_col)
            constrs[rate_col_ind] = 1
        classifier_params['monotone_constraints'] = constrs 

    #Lightgbm sklearn API
    clf = lgb.LGBMClassifier(**classifier_params)
    clf.fit(X_train, y_train, **fit_params)

    #Plot metrics
    lgb.plot_metric(clf, metric=fit_params.get('eval_metric', None))
    plt.savefig(pjoin(output_folder, f'metric_plot{model_name}.png'), bbox_inches='tight')
    
    #Feature importance
    for fi in ['split','gain']:
        lgb.plot_importance(clf, title=f'Feature Importance by {fi}', importance_type=fi, max_num_features=100, figsize=(10,14))
        plt.savefig(pjoin(output_folder, f'importance_plot_{fi}{model_name}.png'), bbox_inches='tight')
    fi_df = get_feature_importance(clf.booster_)
    fi_df.to_csv(pjoin(output_folder, f'variable_importance{model_name}.csv'), index=None)
    
    #Save model
    clf.booster_.save_model(pjoin(output_folder, f'model{model_name}.txt'))
    save_pickle(clf, pjoin(output_folder, f'model{model_name}.pickle'))
    return clf

def score_model(model_file, output_file=None, X=None, y=None, data_file=None, header_file=None, fillna_value=None, use_cat_cols=True, return_y=False):
    """Score rate model and output id_col, prediction and target_col
    
    Input:
        data_file: original data file for prediction
        header_file: header file indicating column type
        model_file: pre-trained model file
        output_file: location to save output data
    """
    if data_file is not None and header_file is not None:
        if return_y:
            X, y = read_prepare_data(header_file, data_file, fillna_value=fillna_value, use_cat_cols=use_cat_cols, return_y=return_y)
        else:
            X = read_prepare_data(header_file, data_file, fillna_value=fillna_value, use_cat_cols=use_cat_cols, return_y=return_y)
    mdl = lgb.Booster(model_file=model_file)
    y_pred = pd.DataFrame(mdl.predict(X))
    if output_file is not None:
        esave(y_pred, output_file)
    if not return_y:
        return y_pred
    else:
        return y_pred, y

def merge_rate_model(data_file, raw_model_output_file, output_file, id_col='id', target_col='signed_ind'):
    """Merge raw rate model output to label and id
    """
    y_pred = eread(raw_model_output_file)
    df = pd.read_csv(data_file, usecols=[id_col, target_col])
    df['rate_pred'] = y_pred
    esave(df, output_file)
    return df

def score_rate_model(data_file, X, model, output_file, id_col='id', target_col='signed_ind', other_cols=[]):
    y_pred = model.predict_proba(X)[:,1]
    df = pd.read_csv(data_file, usecols=[id_col, target_col] + other_cols)
    df['rate_pred'] = y_pred
    esave(df, output_file)
    return df

def score_rate_model_2(data_file, header_file, model_file, output_file, id_col='id', target_col='signed_ind'):
    """wrapper funciton of score_model and merge_rate_model
    """
    X, y = read_prepare_data(header_file, data_file)
    mdl = lgb.Booster(model_file=model_file)
    df = pd.read_csv(data_file, usecols=[id_col, target_col])
    df['rate_pred'] = mdl.predict(X)
    esave(df, output_file)
    return df

def score_term_model(data_file, raw_model_output_file, output_file, 
                     impute_rate_cols, orig_rate_cols, terms, target_term_prob_cols,
                     id_col='id', target_col='initial_term', act_rate_col='interest_rate', loan_amount_col='requested_amount'):
    """From raw prediction populate the final columns
    
    Input:
        data_file: original data file for prediction
        raw_model_output_file: raw prediction from lightgbm
        output_file: location to save output data
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    p = pd.read_csv(raw_model_output_file)
    prob_cols = [rate_col + '_prob' for rate_col in impute_rate_cols]
    p.columns = prob_cols
    df = pd.read_csv(data_file, usecols=[id_col, target_col, act_rate_col, loan_amount_col] + impute_rate_cols + orig_rate_cols + target_term_prob_cols)
    df = pd.merge(df, p, left_index=True, right_index=True)
    df = cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols)
    esave(df, output_file)
    return df

def cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols):
    """Generate the following:
        1. term with highest score
        2. score weighted average term and round to the nearest term in terms
        3. score weighted average rate
        
    Input: 
        df: original data frame and raw prediction
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    rates = df[orig_rate_cols]
    #mask matrix: 0 for missing rates, 1 non-missing
    mask = ~rates.isna() + 0
    adjusted_p = df[prob_cols].values * mask.values
    adjusted_p = pd.DataFrame(adjusted_p)
    
    #1. term with highest score
    adjusted_p_max = adjusted_p.idxmax(axis=1) #index for the maximum probability
    term_map = {idx: term for idx, term in enumerate(terms)}
    df['term_max_prob'] = adjusted_p_max.map(term_map) #map the index to rate
        
    #2. score weighted average term and round to the nearest term in terms
    adjusted_p['sum'] = adjusted_p.sum(axis=1)
    for col in adjusted_p.columns[: -1]:
        adjusted_p[col] = np.where(adjusted_p['sum'] != 0, adjusted_p[col] / adjusted_p['sum'], 0)
    term_p_cols = []
    for idx, term in enumerate(terms):
        term_p_col_nm = '{}_p'.format(term)
        adjusted_p[term_p_col_nm] = adjusted_p[idx] * term
        term_p_cols.append(term_p_col_nm)
    df['term_wgt_prob'] = adjusted_p[term_p_cols].sum(axis=1)
    
    def find_closest_value(val):
        distance = 999999
        answer = -1
        for term in terms:
            if np.abs(val-term) < distance:
                distance = np.abs(val-term)
                answer = term
            else:
                continue
        return answer 
    
    df['term_wgt_prob'] = df['term_wgt_prob'].map(find_closest_value)
    
    #Merge adjusted probability with df
    df = pd.merge(df, adjusted_p.iloc[:, :len(terms)], left_index=True, right_index=True)
    prob_cols_adj = [prob_col + '_adj' for prob_col in prob_cols]
    df.rename(columns={idx: prob_col_adj for idx, prob_col_adj in enumerate(prob_cols_adj)}, inplace=True)
    
    #3. score weighted average rate  
    df['avg_rate'] = np.sum(df[impute_rate_cols].values * df[prob_cols_adj].values, axis=1)
    return df

def load_json(json_file):
    """Read JSON file to dictionary
    """
    with open(json_file, "r") as jfile:
         return json.load(jfile, object_pairs_hook=OrderedDict)
    
def save_json(json_dict, json_file):
    with open(json_file, "w") as f:
        json.dump(json_dict, f, separators=(',\n', ': '))

def chg_json(old_json_file, new_json_file, change_dict=None, del_keys=None):
    """Change old_json_file to new_json_file with change_dict and del_keys
    """
    json_dict = load_json(old_json_file)
    if change_dict is not None:
        for key, value in change_dict.items():
            json_dict[key] = value
    if del_keys is not None:
        for key in del_keys:
            del json_dict[key]
    save_json(json_dict, new_json_file)
          
def make_output_dir(output_dir, name, make=True, make_time_stamp_dir=True):
    """
    Create or name a sub directory inside output_dir   
    """
    #Set the time zone to EST. When using qsub, the default local time is PST
    if make_time_stamp_dir:
        os.environ['TZ'] = 'EST+05EDT,M4.1.0,M10.5.0'
        time.tzset()
        cur_time = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())
        sub_output_dir = pjoin(output_dir, name + '_' + cur_time)
    else:
        sub_output_dir = pjoin(output_dir, name)
    if make==True and not os.path.exists(sub_output_dir):
        os.makedirs(sub_output_dir)
    return sub_output_dir

def save_pickle(obj, pickle_file):
    """Dump obj into pickle_file
    """
    with open(pickle_file, 'wb') as f:
        pickle.dump(obj, f)

def load_pickle(pickle_file):
    """load pickle_file
    """
    with open(pickle_file, 'rb') as f:
        return pickle.load(f)

def read_params(params):
    """Read parameters from dictionary
    """
    fit_params_keys = ['early_stopping_rounds', 'eval_metric', 'eval_names', 'verbose']
    fit_params = {key: params[key] for key in fit_params_keys if key in params}
    classifier_params_keys = ['n_estimators', 'learning_rate', 'max_depth', 'num_leaves', 'min_child_samples', 'n_jobs']
    classifier_params = {key: params[key] for key in classifier_params_keys if key in params}
    return fit_params, classifier_params

def cast_bool_from_json(params, key, default):
    """Cast key from params from string to Boolean
    """
    if key not in params:
        return default
    else:
        if isinstance(params[key], bool):
            return params[key]
        else:
            if params[key] == 'True':
                return True
            elif params[key] == 'False':
                return False

def adjust_term_rate(df, rate_cols=[f'min_rate_{term}' for term in range(2, 8)], term_pred_cols=[f'pred_term_{term}' for term in range(2, 8)]):
    """Adjust term prediction to have 0 on missing rate terms and sum to 1
    """
    mask = ~df[rate_cols].isna() + 0
    df[term_pred_cols] = df[term_pred_cols].values * mask.values
    df['pred_term_sum'] = df[term_pred_cols].sum(axis=1)
    for col in term_pred_cols:
        df[col] = np.where(df['pred_term_sum'] != 0, df[col] / df['pred_term_sum'], 0)
    return df

def get_segment_params(param_names, segment):
    """Load mnlogit model parameters for each product segment (i.e. term 2 - 7)
    """
    result = []
    for item in param_names:
        if re.search(f':{segment}', item):
            result.append(item)
    return result