#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:26 2019

@author: esun

Utility functions for data query and processing
"""
from os.path import splitext
from sqlalchemy import create_engine
try:
    import mysql
except:
    pass
try:
    import snowflake.connector
except:
    pass
import time
import numpy as np
import pandas as pd

def query_snowflake(qry):
    """Return the pandas DataFrame from input SQL qry with connection to snowflake
    """
    start = time.time()
    sc = snowflake.connector.connect(user='sdm_ro', password='none', port=1444, host='localhost', account='sofi', protocol='http')
    df = pd.read_sql(qry, sc)
    sc.close()
    df.columns = df.columns.str.lower()
    end = time.time()
    print(f'snowflake in {end - start}[s]')
    return df

def query_data(qry, data_base):
    assert data_base in ['products', 'sofidw', 'core_db'], "data_base must be from ['products', 'sofidw', 'core_db']!"
    if data_base == 'products':
        my_db = 'postgresql://localhost:15551/products-prod-ro'
    elif data_base == 'sofidw':
        my_db = 'postgresql://localhost:15501/dw-prod-sofidw-ro'
    else:
        my_db = 'mysql+mysqlconnector://:@localhost:13309/sofi'
    start = time.time()
    engine = create_engine(my_db, echo=True)
    df = pd.read_sql_query(qry, con=engine)
    end = time.time()
    print(f'{data_base} in {end - start}[s]')
    return df

def divide_train_oos(df, random_state=None, frac=0.7):
    """Divide df into frac for training and 1 - frac for oos data
    """
    train = df.sample(frac=0.7, random_state=random_state)
    test = df.drop(train.index)
    return train, test

def add_signed_ind(df, model_target='signed_ind', term_var='initial_term', terms=None, filterdays=30, remove_mix_labels=True):
    """Set the signed indicator by terms and filter out the ones with signed timewindow > filterdays
    Also change target variable accordingly by timewindow
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['date_signed'] = pd.to_datetime(df['date_signed'])
    if terms is None: 
        #Infer terms list from "initial_term" variable
        terms = df['initial_term'].unique() #take distinct term values as a numpy array
        terms = sorted(terms[~np.isnan(terms)].astype(int))
    for term in terms:
        df[model_target + '_{0}'.format(term)] = np.where((df[model_target] == 1) & (df[term_var] == term) & 
                                                          (df['date_signed'] - df['date_start'] <= pd.Timedelta(filterdays,'D')), 1, 0)
    df[model_target] = np.where(df['date_signed'] - df['date_start'] > pd.Timedelta(filterdays,'D'), 0, df[model_target])
    if remove_mix_labels:
        df = df[~(df['date_signed'] - df['date_start'] > pd.Timedelta(filterdays,'D'))]
    return df

def bin_yoe(x):
    """Bucket years of experience
    """
    if pd.isnull(x):
        return 'null'
    elif x == 0:
        return '0'
    elif x <= 5:
        return '1_to_5'
    elif x <= 10:
        return '6_to_10'
    else:
        return 'over_10'
    
def clean_str(str_col):
    """Return lower case of str_col and replace ' ' and '-' with '_'
    """
    str_col = str_col.str.lower()
    str_col = str_col.str.replace(" ",'_')
    str_col = str_col.str.replace("-",'_')
    return str_col

def map_term_to_rate(x, term_col='initial_term', rate_cols=['rate_5','rate_7','rate_10','rate_15','rate_20'], terms=[5, 7, 10, 15, 20]):
    """Read interest rate from rate_cols based on 'initial_term' selected by the customer
    """
    rate_map = {term: idx for idx, term in enumerate(terms)}
    term = x[term_col]
    if np.isnan(term):
        return np.nan
    else:
        rate_cols_idx = rate_map[term]
        return x[rate_cols[rate_cols_idx]]

def flat_rate(df, terms, term_col='initial_term', rate_col='interest_rate'):
    """Create columns to map interest_rate and initial term to individual term columns
    """
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    for term, col in zip(terms, rate_map_cols):
        df[col] = np.where(df[term_col] == term, df[rate_col], 0)
    return df

def impute_rate(df, rate_cols=['rate_5','rate_7','rate_10','rate_15','rate_20'], impute_rate_dict=None, keep_orig=True):
    """Impute rate_cols by maximum of rate_cols if impute_rate is None, else by impute_rate dictionary
    """
    if impute_rate_dict is None:
        impute_rate_dict = df[rate_cols].max().to_dict()
    if keep_orig:
        for rate_col in rate_cols:
            df[rate_col + '_orig'] = df[rate_col]
    df[rate_cols] = df[rate_cols].fillna(value=impute_rate_dict)
    return df, impute_rate_dict

def flat_term_rate_dat(df_orig, flat_cols, term_col='product_term', 
                       id_col='id', term_values=[2, 3, 4, 5, 6, 7],
                       consolidate_cols=['tier'], rename_postfix=None):
    """Flatten the data based on term_col
    """
    df_flat = df_orig.copy()
    df_flat = df_flat[df_flat[term_col].isin(term_values)]
    df_flat = df_flat.pivot_table(index=id_col, columns=term_col, values=flat_cols)
    df_flat.columns = ['_'.join([col_level_0, str(col_level_1)]) for col_level_0, col_level_1 in zip(list(df_flat.columns.get_level_values(0)), list(df_flat.columns.get_level_values(1)))]
    for consolidate_col in consolidate_cols:
        orig_cols = [consolidate_col + '_{}'.format(term) for term in [2, 3, 4, 5, 6, 7]]
        df_flat[consolidate_col] = df_flat[orig_cols].max(axis=1, skipna=True)
        df_flat.drop(columns=orig_cols, inplace=True)
    if rename_postfix is not None:
        df_rename_dict = {col: col + '_' + rename_postfix for col in df_flat.columns}
        df_flat.rename(columns=df_rename_dict, inplace=True)
    return df_flat

def calc_monthly_payments(amt, term, rate):
    """Calculate monthly paymet based on loan_amt, term and rate
    """
    r = 1 + (rate / 12)
    e = (-1*term*12)
    div = 1 - (r ** e)
    mult = rate / 12 / div
    return amt * mult

def calc_payments(amt, term, rate):
    """Calculate total payment and monthly paymet based on loan_amt, term and rate
    """
    r = 1 + (rate / 12)
    e = (-1*term*12)
    div = 1 - (r ** e)
    yearly_payment = amt * rate / div
    monthly_payment = yearly_payment / 12
    total_payment = yearly_payment * term
    return monthly_payment, total_payment

def map_cat_to_num(df_orig, target_col, cat_var_col, change_df_orig=True):
    """Map categorical variable to numerical variable
    For each category, replace original value by target_col's mean on this category
    If observation count is less than 30 for a category, replace with global mean
    """
    if not change_df_orig:
        df = df_orig.copy()
    else:
        df = df_orig
    var_cnt = pd.DataFrame(df[cat_var_col].value_counts())
    target_var_means = []
    overall_mean = df[target_col].mean()
    for idx in var_cnt.index:
        if var_cnt.loc[idx, cat_var_col] >= 30:
            target_var_mean = df[df[cat_var_col] == idx][target_col].mean()
        else:
            target_var_mean = overall_mean
        target_var_means.append(target_var_mean)
    map_dict = {idx: target_var_mean for idx, target_var_mean in zip(var_cnt.index, target_var_means)}
    df[cat_var_col + '_num'] = df[cat_var_col].map(map_dict)
    if not change_df_orig:
        return df[cat_var_col + '_num']
    else:
        return df

def eread(data_file):
    """Utility function to have same interface for reading csv and feather data
    """
    data_file_ext = splitext(data_file)[-1]
    if data_file_ext == ".csv":
        df = pd.read_csv(data_file)
    elif data_file_ext == ".feather":
        df = pd.read_feather(data_file)
    return df

def esave(df, data_file):
    """Utility function to have same interface for saving csv and feather data
    """
    data_file_ext = splitext(data_file)[-1]
    if data_file_ext == ".csv":
        df.to_csv(data_file, index=None)
    elif data_file_ext == ".feather":
        try:
            df.to_feather(data_file)
        except ValueError:
            df.reset_index(drop=True).to_feather(data_file)

def emerge(dfs, on):
    """Merge multiple dfs 
    """     
    df = dfs[0]
    for df_iter in dfs[1:]:
        df = pd.merge(df, df_iter, on=on)
    return df    
    
def read_grid(grid_file, rate_col_nm='grid_rate'):
    """Read base_grid and overlay_grid into one DataFrame
    """
    base_grid = pd.read_excel(grid_file, sheet_name='tier_term')
    overlay_grid = pd.read_excel(grid_file, sheet_name='fico_loan')
    
    overlay_grid_repeat = pd.DataFrame()
    for tier in range(1, 8):
        overlay_grid_iter = overlay_grid.copy()
        overlay_grid_iter['tier'] = tier
        overlay_grid_repeat = pd.concat([overlay_grid_repeat, overlay_grid_iter], axis=0)
        
    grid = pd.merge(base_grid, overlay_grid_repeat, on='tier')
    for term in range(2, 8):
        grid[f'{rate_col_nm}_{term}'] = grid[f'{term}_x'] + grid[f'{term}_y']
    grid = grid[['tier', 'fico_bin', 'loan_amt_bin'] + [f'{rate_col_nm}_{term}' for term in range(2, 8)]]
    return grid

def add_postfix_colname(df, postfix, change_cols=None):
    """Add postfix to given columns of a dataframe df
    """
    if change_cols is None:
        change_cols = df.columns
    df.rename(columns={col: col + '_' + postfix for col in change_cols}, inplace=True)
    return df

def check_trans_to_list(x):
    """Check if x is an instance of list, if not, transform it into a list
    """
    if not isinstance(x, list):
        return [x]
    else:
        return x
    
def change_cols(orig_header_file, new_header_file, remove_cols=None, add_cols=None, add_type='N'):
    """Based on orig_header_file, create new_header_file with removing remove_cols and add add_cols
    """
    new_header = pd.read_csv(orig_header_file)
    if remove_cols is not None:
        remove_cols = check_trans_to_list(remove_cols)
        for var in remove_cols:
            new_header.at[0, var] = 'I'
    if add_cols is not None:
        add_cols = check_trans_to_list(add_cols)
        for var in add_cols:
            if var in new_header.columns:
                new_header.at[0, var] = add_type
            else: 
                new_header[var] = add_type
    new_header.to_csv(new_header_file, index=None)
    
def create_fico_loan_bin(df):
    """Create fico and loan amount bin for a given dataframe. The bin is according to price grid fico loan amount overlay
    """
    credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
    df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
    df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                          np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))
    return df

def merge_grid_price(df, grid, terms, rate_col_nm='grid_rate', tier_col_nm='tier'):
    """Merge price from grid
    """
    for term in terms:
        df_term = pd.merge(df.loc[df[f'{tier_col_nm}_{term}'].notnull(), ['id', f'{tier_col_nm}_{term}', 'fico_bin', 'loan_amt_bin']], 
                           grid[['tier','fico_bin', 'loan_amt_bin', f'{rate_col_nm}_{term}']], 
                           left_on=[f'{tier_col_nm}_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
        df = pd.merge(df, df_term[['id', f'{rate_col_nm}_{term}']], on='id', how='left')
    return df

def cut_cat_var(df, cat_var, cat_var_value_list):
    """For a given cat_var in a DataFrame df, keep values in cat_var_value_list and group other values in 'Rest'
    """
    df[cat_var] = np.where((df[cat_var].isin(cat_var_value_list)) | (df[cat_var].isnull()), df[cat_var], 'Rest')
    return df

def check_tier_dist(df, tier_col='tier_pre', no_tier=5):
    """Calculate tier distribution by week
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_date = df['date_start'].min()
    max_date = df['date_start'].max()
    weeks = (max_date-min_date).days // 7
    start_dts = []
    end_dts = [max_date - pd.DateOffset(7*cnt) for cnt in range(weeks, -1, -1)]
    for idx, end_dt in enumerate(end_dts):
        if idx > 0:
            start_dts.append(end_dt - pd.DateOffset(6))
        else:
            start_dts.append(min_date)
    tier_dist_result = []
    tier_dist_cnt_result = []
    for date_start, date_end in zip(start_dts, end_dts):
        df_tmp = df[(df['date_start'] >= date_start) & (df['date_start'] <= date_end)]
        tier_dist = list(df_tmp.groupby(tier_col)['requested_amount'].sum() / df_tmp['requested_amount'].sum())
        tier_dist_cnt = list(df_tmp.groupby(tier_col)['id'].count() / df_tmp['id'].count())
        tier_dist_result.append(tier_dist)
        tier_dist_cnt_result.append(tier_dist_cnt)
    tier_dist_df = pd.DataFrame(tier_dist_result).transpose()
    tier_dist_cnt_df = pd.DataFrame(tier_dist_cnt_result).transpose()
    #tier_dist_df.columns = start_dts
    tier_dist_df[tier_col] = np.arange(1, no_tier + 1)
    tier_dist_cnt_df[tier_col] = np.arange(1, no_tier + 1)
    return tier_dist_df, start_dts, end_dts, tier_dist_cnt_df

def apply_tier_dist_chg(df, tier_dist_df, start_dts, end_dts, tier_col='tier_pre'):
    """Adjust requested_amount to change tier distribution in optimization data
    """
    for idx, (date_start, date_end) in enumerate(zip(start_dts[:-1], end_dts[:-1])):
        for tier in range(1, 6):
            df.loc[(df['date_start'] >= date_start) & (df['date_start'] <= date_end) & (df[tier_col] == tier), 'requested_amount'] \
            *= (tier_dist_df.loc[tier_dist_df[tier_col] == tier, len(start_dts)-1] / tier_dist_df.loc[tier_dist_df[tier_col] == tier, idx]).values[0]
    return df

def map_tier(row):
    """Map 5 tiers to 9 tiers
    """
    if row['tier_pre'] == 1:
        if row['fcf_post_max'] >= 6000 and row['ml_score'] > 76.05:
            return 1
        else:
            return 2
    elif row['tier_pre'] == 2:
        if row['fcf_post_max'] >= 5000:
            return 3
        else:
            return 4
    elif row['tier_pre'] == 3:
        if row['fcf_post_max'] >= 4000:
            return 5
        else:
            return 6
    elif row['tier_pre'] == 4:
        if row['fcf_post_max'] >= 4000:
            return 7
        else:
            return 8
    else:
        return 9
